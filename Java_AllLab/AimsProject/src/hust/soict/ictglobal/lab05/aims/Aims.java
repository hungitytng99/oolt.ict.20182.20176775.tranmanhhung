package hust.soict.ictglobal.lab05.aims;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Aims {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Order anOrder = new Order();
		
		//boolean addDigitalVideoDisc(DigitalVideoDisc disc)
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Atlers");
		dvd1.setLength(87);
		anOrder.addDigitalVideoDisc(dvd1);
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Ficton");
		dvd2.setCost(24.95f);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);
		anOrder.addDigitalVideoDisc(dvd2);
		
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladin");
		dvd3.setCategory("Animation");
		dvd3.setCost(18.99f);
		dvd3.setDirector("John Musker");
		dvd3.setLength(90);
		anOrder.addDigitalVideoDisc(dvd3);
		
		DigitalVideoDisc dvd4 = new DigitalVideoDisc("Venom");
		dvd4.setCategory("Animation");
		dvd4.setCost(50.05f);
		dvd4.setDirector("Ruben Fleischer");
		dvd4.setLength(120);
		
		//void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2)
		DigitalVideoDisc dvd5 = new DigitalVideoDisc("Independence Day");
		dvd5.setCategory("Science");
		dvd5.setCost(30.15f);
		dvd5.setDirector("Roland Emmerich");
		dvd5.setLength(120);
		anOrder.addDigitalVideoDisc(dvd4,dvd5);
		
		//Add DVD by array "boolean addDigitalVideoDisc(DigitalVideoDisc[] dvdList)"
		DigitalVideoDisc dvdTemp1 = new DigitalVideoDisc("title");
		DigitalVideoDisc [] dvdList = new DigitalVideoDisc[3]; 
		
		dvdTemp1.setTitle("Aquaman");
		dvdTemp1.setCategory("Fiction");
		dvdTemp1.setCost(27.11f);
		dvdTemp1.setDirector("James Wan");
		dvdTemp1.setLength(143);
		dvdList[0] = dvdTemp1;
		
		DigitalVideoDisc dvdTemp2 = new DigitalVideoDisc("title");
		dvdTemp2.setTitle("Mute");
		dvdTemp2.setCategory("Supernatural");
		dvdTemp2.setCost(20.96f);
		dvdTemp2.setDirector("Xing Fei");
		dvdTemp2.setLength(126);
		dvdList[1] = dvdTemp2;
		
		DigitalVideoDisc dvdTemp3 = new DigitalVideoDisc("title");
		dvdTemp3.setTitle("Overlord");
		dvdTemp3.setCategory("Horror");
		dvdTemp3.setCost(17.11f);
		dvdTemp3.setDirector("Julius Avery");
		dvdTemp3.setLength(110);
		dvdList[2] = dvdTemp3;
		anOrder.addDigitalVideoDisc(dvdList);
		
		Date CurrentDate = new Date();
		anOrder.setDateOrdered(CurrentDate.toString());
        System.out.print("Total cost is : ");
        System.out.println(anOrder.totalCost());
        System.out.println("<Remove dvd3:>");
        anOrder.removeDigitalVideoDisc(dvd3);
        System.out.println("Total cost is : "+ anOrder.totalCost());
        anOrder.printOrders();
        
	}

}
