package hust.soict.ictglobal.lab05.aims;

public class DiskTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//"boolean search(String title)" in DigitalVideoDisc??????
		Order anOrder = new Order();
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Atlers");
		dvd1.setLength(87);
		anOrder.addDigitalVideoDisc(dvd1);
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Ficton");
		dvd2.setCost(24.95f);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);
		anOrder.addDigitalVideoDisc(dvd2);
		
        System.out.println("");
        System.out.println("Search(String title) in DigitalVideoDisc: ");
        if(dvd1.search("The    Lion  King    ")) System.out.println("Founded \"The    Lion  King    \"");
        else System.out.println(">>>Not Founded \"The    Lion  King    \".");
        
        //DigitalVideoDisc getALuckyItem()
        System.out.println("");
        System.out.println(">>>> Get A Lucky Item ( Cost = 0) <<<<");
        System.out.println("+++ Item free is: " + anOrder.getALuckyItem().getTitle()+ " +++");
        anOrder.printOrders();
        System.out.println("");
	}

}
