package hust.soict.ictglobal.lab05.aims;

import java.util.Random;

public class Order {
	public static final int MAX_NUMBER_ORDERED = 10;
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBER_ORDERED];
	private int qtyOrdered=0;
	public int getQtyOrdered() {
		return qtyOrdered;
	}
	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	private String  dateOrdered;
	public String getDateOrdered() {
		return dateOrdered;
	}
	public void setDateOrdered(String dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
	boolean addDigitalVideoDisc(DigitalVideoDisc disc)
	{
		if(getQtyOrdered()>(MAX_NUMBER_ORDERED-1)) 
			{
			System.out.println(">>FULL: The disc: " + disc.getTitle()+ "couldn't be added");
			return false;
			}
		itemsOrdered[qtyOrdered]=disc;
		qtyOrdered+=1;
		return true;
	}
	boolean addDigitalVideoDisc(DigitalVideoDisc[] dvdList)
	{
		for(DigitalVideoDisc dvd: dvdList)
		{
			try
			{
			if(qtyOrdered>MAX_NUMBER_ORDERED) return false;
			else
			{
			itemsOrdered[qtyOrdered] = dvd;
			qtyOrdered++;
			}
			}catch(ArrayIndexOutOfBoundsException aiob)
			{
				System.out.println(">>FULL: The disc: "+ dvd.getTitle() +" wasn't added.");
			}
		}
		return true;
	}
	void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2)
	{
		addDigitalVideoDisc(dvd1);
	    addDigitalVideoDisc(dvd2);
	}
	void removeDigitalVideoDisc(DigitalVideoDisc disc)
	{
		int i;
		for(i=0;i<qtyOrdered;i++)
		{
			if(itemsOrdered[i]==disc) break;
		}
		for(int j=i+1;j<qtyOrdered;i++,j++)
			itemsOrdered[i]=itemsOrdered[j];
		//itemsOrdered[qtyOrdered-1] = new DigitalVideoDisc();
		qtyOrdered--;
	}
	float totalCost()
	{
		float Sum=0;
		for(int j=0;j<qtyOrdered;j++) 
		{
			Sum = Sum + itemsOrdered[j].getCost();
			}
		return Sum;
	}
	void printOrders()
	{
		System.out.println("*************************Orders***************************");
		System.out.println("\u001BDate : " + dateOrdered);
		System.out.println("\u001BOrders Items:");
		for(int i=0;i<qtyOrdered;i++)
		{
			System.out.println((i+1) +". DVD - "+itemsOrdered[i].getTitle()+" - "+itemsOrdered[i].getCategory()+" - "+itemsOrdered[i].getDirector()+" - "+itemsOrdered[i].getLength()+": "+itemsOrdered[i].getCost()+"$"); 
		}
		System.out.println("\u001BTotal cost: " + totalCost());
		System.out.println("************************************************************");
	}
	DigitalVideoDisc getALuckyItem()
	{
		Random rd = new Random();
		int luckyNum = rd.nextInt(qtyOrdered);
		itemsOrdered[luckyNum].setCost(0.0f);
		return itemsOrdered[luckyNum];
	}
}
