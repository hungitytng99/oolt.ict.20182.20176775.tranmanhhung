package hust.soict.ictglobal.lab06.aims.media;

import java.io.ObjectInputStream.GetField;
import java.util.Scanner;

public class Aims {
	public static void showMenu()
	{
		System.out.println("Order Managament Application: ");
		System.out.println("----------------------------------");
		System.out.println("1.Create new order.");
		System.out.println("2.Add item to the order.");
		System.out.println("3.Delete item by id.");
		System.out.println("4.Display the items list of order.");
		System.out.println("0.Exit.");
		System.out.println("----------------------------------");
		System.out.print("Please choose a number(0-1-2-3-4):   ");
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		Order anOrder = new Order();
		while(true)
		{
			showMenu();
			int n = sc.nextInt();
			switch(n)
			{
			case 1:
			{
				anOrder.getItemsOrdered().clear();
				System.out.println("Ready to create new order.");
				System.out.println("");
				break;
			}
			case 2:
			{
				anOrder.addMedia();
				System.out.println("Success!");
				System.out.println("");
				break;
			}
			case 3:
			{
				int check = 0;
				String id;
				System.out.print("Input id need to delete: ");String clear = sc.nextLine();
				id = sc.nextLine();
				Media aMedia = new Media();
				for(int i = 0; i<anOrder.getItemsOrdered().size();i++)
				{
					aMedia = anOrder.getItemsOrdered().get(i);
					if(aMedia.getId().compareToIgnoreCase(id) == 0)
					{
					System.out.println("Clear successfully !");
					aMedia = anOrder.getItemsOrdered().remove(i);
					check = 1;
					}
				}
				if(check == 0) System.out.println("NOT FOUND !!!");
				break;
			}
			case 4:
			{
				System.out.println(">>> The ordered list: ");
				for(int i = 0; i<anOrder.getItemsOrdered().size();i++)
				{
					Media aMedia = new Media();
					aMedia = anOrder.getItemsOrdered().get(i);
					System.out.println(aMedia.getId() + " - " + aMedia.getTitle() + " - " + aMedia.getCategory() + " - " + aMedia.getCost() + "$");
				}
				break;
			}
			case 0:
			{
				System.out.println("EXIT.");
				break;
			}
			}
			if(n == 0) break;
		}
	}

}
