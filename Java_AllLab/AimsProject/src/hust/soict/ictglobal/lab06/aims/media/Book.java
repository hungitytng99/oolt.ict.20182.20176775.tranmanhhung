package hust.soict.ictglobal.lab06.aims.media;
import java.util.*;
public class Book extends Media {
	Scanner input = new Scanner(System.in);
	private List<String> authors = new ArrayList<String>();
	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	void addAuthor(String authors)
	{
		for(String str : this.authors)
		{
			if(str.compareToIgnoreCase(authors)==0)
			{
				System.out.println("Author is aready available.");
				return;
			}
		(this.authors).add(authors);
		}
	}
	void removeAuthor(String authors)
	{
		if((this.authors).indexOf(authors)==-1)
		{
			System.out.println("This author doesnot have in list.");
			return;
		}
		(this.authors).remove(this.authors.indexOf(authors));
	}
	public Book() {
		// TODO Auto-generated constructor stub
		super();
	}
	public Book(String id) {
		super(id);
	}
	
	public Book(String id,String title)
	{
		super(id,title);
	}
	public Book(String id,String title, String category)
	{
		super(id,title,category);
	}
	public Book(String id,String title, String category,float cost)
	{
		super(id,title,category,cost);
	}
	public Book(String id,String title,String category,float cost, List<String> authors)
	{
		super(id,title, category,cost);
		this.authors = authors;
	}
	

}
