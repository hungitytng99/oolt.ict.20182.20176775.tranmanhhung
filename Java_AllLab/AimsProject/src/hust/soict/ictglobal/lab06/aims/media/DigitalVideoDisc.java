package hust.soict.ictglobal.lab06.aims.media;
public class DigitalVideoDisc extends Media{
	private String director;
	private int length;
	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	// Constructor
	public DigitalVideoDisc() {
		super();
	}
	public DigitalVideoDisc(String id) {
		super(id);
	}
	public DigitalVideoDisc(String id,String title) {
		super(id,title);
	}
	public DigitalVideoDisc(String id,String title,String category) {
		super(id,title,category);
	}
	public DigitalVideoDisc(String id,String title,String category,float cost) {
		super(id,title,category,cost);
	}
	public DigitalVideoDisc(String id,String title,String category,float cost,String director) {
		super(id,title,category,cost);
		this.director = director;
	}
	public DigitalVideoDisc(String id,String title,String category,float cost,String director,int length) {
		super(id,title,category,cost);
		this.director = director;
		this.length = length;
	}
	
}
