package hust.soict.ictglobal.lab07.aims.media;

import java.util.ArrayList;
import java.util.Scanner;


public class Aims {
	public static void showMenu()
	{
		System.out.println("Order Managament Application: ");
		System.out.println("----------------------------------");
		System.out.println("1. Clear all available media.");
		System.out.println("2. Add item to the media.");
		System.out.println("3. Display available media");
		System.out.println("0.Exit.");
		System.out.println("----------------------------------");
		System.out.print("Please choose a number(0-1-2-3-4):   ");
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<CompactDisc> cd = new ArrayList<CompactDisc>();
		ArrayList<DigitalVideoDisc> dvd = new ArrayList<DigitalVideoDisc>();
		ArrayList<Book> book = new ArrayList<Book>();
		Order anOrder = new Order();
		while(true)
		{
			showMenu();
			Scanner scc = new Scanner(System.in);
			int n = scc.nextInt();
			switch(n)
			{
			case 1:
			{
				cd.clear();
				dvd.clear();
				book.clear();
				System.out.println("Ready to create new media.");
				System.out.println("");
				break;
			}
			case 2:
			{
				while(true)
				{
				Scanner sc1 = new Scanner(System.in);
				System.out.println("> Choose kind of media: ");
				System.out.println("1.Book.");
				System.out.println("2.Compact Disc (CD).");
				System.out.println("3.Digital Video Disc (DVD).");
				System.out.println("4.End.");
				System.out.print("Your choice: ");
				int choice = sc1.nextInt();
				switch(choice)
				{
				case 1:
				{
					ArrayList<String> authors = new ArrayList<String>();
					int check =0;
					Scanner sc = new Scanner(System.in);
					Book tempBook = new Book();
					System.out.println(">  Input information for book: ");
					System.out.println("Title: ");
					tempBook.setTitle(sc.nextLine());
					System.out.println("Category: ");
					tempBook.setCategory(sc.nextLine());
					do {
						System.out.println("Authors: ");
						authors.add(sc.nextLine());
						System.out.println("Press 1 to continue input!!");
						check = Integer.valueOf(sc.nextLine());
					}while(check == 1);
					tempBook.setAuthors(authors);
					System.out.println("Cost: ");
					tempBook.setCost(sc.nextFloat());
					book.add(tempBook);
					break;
				}
				case 2:
				{
					Scanner sc = new Scanner(System.in);
					CompactDisc tempCd = new CompactDisc();
					System.out.println(">  Input information for CD: ");
					System.out.println("Title: ");
					tempCd.setTitle(sc.nextLine());
					System.out.println("Artist: ");
					tempCd.setArtist(sc.nextLine());
					System.out.println("Do you want to add track?(input 1 to confirm) ");
					int check = sc.nextInt();
					while(check == 1)
					{
						tempCd.addTrack();
						System.out.println(" Press 1 to continue input!!");
						check = sc.nextInt();
					}
					cd.add(tempCd);
					break;
				}
				case 3:
				{
					Scanner sc = new Scanner(System.in);
					DigitalVideoDisc tempDvd = new DigitalVideoDisc();
					System.out.println(">  Input information for DVD: ");
					System.out.println("Title: ");
					tempDvd.setTitle(sc.nextLine());
					System.out.println("Director: ");
					tempDvd.setDirector(sc.nextLine());
					System.out.println("Length: ");
					tempDvd.setLength(sc.nextInt());
					dvd.add(tempDvd);
					break;
				}
				case 4:
				{
					System.out.println("End.");
					break;
				}
				}
				if (choice == 4) break;
				}
				break;
			}
			case 3:
			{
				int choice;
				while(true)
				{
					Scanner sc = new Scanner(System.in);
					System.out.println("> Choose kind of media: ");
					System.out.println("1.Book.");
					System.out.println("2.Compact Disc (CD).");
					System.out.println("3.Digital Video Disc (DVD).");
					System.out.println("4.All.");
					System.out.println("5.End.");
					System.out.print("> Your selection: ");
					choice = sc.nextInt();
					switch(choice)
					{
					case 1:
					{
						int i = 0;
						System.out.println(">> List of available book: ");
						for(Book tempBook : book)
						{
							tempBook.display(i+1);
							i++;
						}
						break;
					}
					case 2:
					{
						int i=0;
						System.out.println(">> List of available cd: ");
						for(CompactDisc tempCd : cd)
						{
							tempCd.display(i+1);
							i++;
						}
					}
					case 3:
					{
						int i=0;
						System.out.println(">> List of available dvd: ");
						for(DigitalVideoDisc tempDvd : dvd)
						{
							tempDvd.display(i+1);
							i++;
						}
						
					}
					case 4: 
					{
						System.out.println();
						System.out.println("All available media: ");
						int i=0;
						System.out.println(">> List of available book: ");
						for(Book tempBook : book)
						{
							tempBook.display(i+1);
							i++;
						}
						System.out.println(">> List of available cd: ");
						for(CompactDisc tempCd : cd)
						{
							tempCd.display(i+1);
							i++;
						}
						System.out.println(">> List of available dvd: ");
						for(DigitalVideoDisc tempDvd : dvd)
						{
							tempDvd.display(i+1);
							i++;
						}
						
					}
						
					}
					if(choice == 5) break;
				}
				break;
			}
			case 0:
			{
				System.out.println("EXIT.");
				break;
			}
			}
			if(n == 0) break;
		}
		
	}

}
