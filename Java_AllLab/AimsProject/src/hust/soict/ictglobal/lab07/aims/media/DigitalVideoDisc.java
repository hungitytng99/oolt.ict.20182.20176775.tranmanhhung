package hust.soict.ictglobal.lab07.aims.media;
public class DigitalVideoDisc extends Disc implements Playable,Comparable{
	// Constructor
	public DigitalVideoDisc() {
	}
	public DigitalVideoDisc(String title)
	{
		this.setTitle(title);
	}
	public void play()
	{
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}
	public void display(int i)
	{
		System.out.println(">> List of available dvd: ");
		System.out.println(i+ ". "+this.getTitle()+" - "+this.getDirector()+" - "+ this.getLength());
	}
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		DigitalVideoDisc dvd = (DigitalVideoDisc)o;
		return (this.getTitle().compareToIgnoreCase(dvd.getTitle()));
	}
}
