package hust.soict.ictglobal.lab07.aims.media;

public interface Playable {
	public void play();
}
