package hust.soict.ictglobal.lab08.aims.media;

public class MemoryDaemon implements Runnable {
	long memoryUsed = 0;
	public void run()
	{
		Runtime rt = Runtime.getRuntime();
		long used;
		for(int i=0;i<10;i++)
		{
			used = rt.totalMemory() - rt.freeMemory();
			if(used != memoryUsed)
			{
				System.out.println("\tMemory used = "+ used);
			}
		}
	}
}
