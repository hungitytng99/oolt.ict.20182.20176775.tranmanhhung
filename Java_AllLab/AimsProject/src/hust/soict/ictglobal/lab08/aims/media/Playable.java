package hust.soict.ictglobal.lab08.aims.media;

public interface Playable {
	public void play();
}
