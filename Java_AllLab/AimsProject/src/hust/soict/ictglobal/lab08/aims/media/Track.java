package hust.soict.ictglobal.lab08.aims.media;

public class Track implements Playable,Comparable{
	private String title;
	private int length = 0;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public void play()
	{
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}
	public void play1()
	{
		System.out.println("Title: " + this.getTitle());
		System.out.println("Length: " + this.getLength());
	}
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		Track tracks = (Track) o;
		return (this.getTitle().compareToIgnoreCase(tracks.getTitle()));
	}
	
}
