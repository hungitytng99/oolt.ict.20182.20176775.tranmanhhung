package soict.hust.ictglobal.gui.awt;

import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class AWTAccumulator extends Frame implements ActionListener,WindowListener{
	private Label lblInput; // Declare input Label
	private Label lblOutput; // Declare output Label
	private TextField tfInput; // Declare input TextField
	private TextField tfOutput; // Declare output TextField
	private int sum = 0; // Accumulated sum, init to 0
	public AWTAccumulator()
	{
		setLayout(new FlowLayout());
		lblInput = new Label("Enter an integer : ");
		add(lblInput);
		tfInput = new TextField(10);
		add(tfInput);
		tfInput.addActionListener(this);
		lblOutput = new Label("The Accumulated Sum is: ");
		add(lblOutput);
		tfOutput = new TextField(10);
		tfOutput.setEditable(false);
		add(tfOutput);
		addWindowListener(this);
		setTitle("AWT Accmulator");
		setSize(350,120);
		setVisible(true);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new AWTAccumulator();
	}
	@Override
	public void actionPerformed(ActionEvent evt) {
		// TODO Auto-generated method stub
		int numberIn = Integer.parseInt(tfInput.getText());
		sum += numberIn;
		tfInput.setText("");
		tfOutput.setText(sum + "");
	}
	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		System.out.println("actived");
	}
	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		System.out.println("Closed");
	}
	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
		System.exit(0);
	}
	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		System.out.println("deactiveed");
	}
	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		System.out.println("Deiconified");
	}
	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		System.out.println("Iconified");
	}
	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		System.out.println("opened");
	}
	

}
