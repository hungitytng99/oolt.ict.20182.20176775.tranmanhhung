package soict.hust.ictglobal.gui.awt;

import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
public class AWTCounter extends Frame{
	private Label lblCount;
	private TextField tfCount;
	private Button CountUp,CountDown,Reset;
	private int count = 0;
	public AWTCounter()
	{
		setLayout(new FlowLayout());
		lblCount = new Label("Counter");
		add(lblCount);
		tfCount = new TextField(count + "",10);
		tfCount.setEditable(false);
		add(tfCount);
		BtnListener listener = new BtnListener();
		CountUp = new Button("Count Up");
		CountDown = new Button("Count Down");
		Reset = new Button("Reset");
		add(CountDown);
		add(CountUp);
		add(Reset);
		CountUp.addActionListener(listener);
		CountDown.addActionListener(listener);
		Reset.addActionListener(listener);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent evt) {
				// TODO Auto-generated method stub
				System.exit(0);
			}
		});
		setTitle("AWT Counter");
		setSize(400,80);
		setVisible(true);
		
	}
	public static void main(String [] args)
	{
		AWTCounter app = new AWTCounter();
	}
	private class BtnListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent evt)
		{
			String btnLabel = evt.getActionCommand();
			if(btnLabel.equals("Count Up"))
			{
				++count;
			}
			else if(btnLabel.equals("Count Down"))
			{
				--count;
			}else
			{
				count = 0;
			}
			tfCount.setText(count + "");
		}
	}
}
