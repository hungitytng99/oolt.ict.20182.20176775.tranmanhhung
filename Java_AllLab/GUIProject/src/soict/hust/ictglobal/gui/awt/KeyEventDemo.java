package soict.hust.ictglobal.gui.awt;

import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyEventDemo extends Frame implements KeyListener{
	private TextField tfInput;
	private TextArea taDisplay;
	private int count = 0;
	public KeyEventDemo() {
		setLayout(new FlowLayout());
		add(new Label("Enter text: "));
		tfInput = new TextField(10);
		add(tfInput);
		taDisplay = new TextArea(5,40);
		add(taDisplay);
		
		tfInput.addKeyListener(this);
		setTitle("KeyEvent Demo");
		setSize(400,200);
		setVisible(true);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new KeyEventDemo();
	}
	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println("keyPressed");
	}
	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println("keyReleased");
	}
	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		count++;
		taDisplay.append(count+ ".You have typed : \"" + arg0.getKeyChar()+" \"\n");
	}

}
