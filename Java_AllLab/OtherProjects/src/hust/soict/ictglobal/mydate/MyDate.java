package hust.soict.ictglobal.mydate;
import java.util.Date;
import java.util.Calendar; 
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Scanner;
public class MyDate {
	int day,month,year;
	public int getDay() {
		return day;
	}
	static Scanner input = new Scanner(System.in);
	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	public void setMonth(String month)
	{
		this.month=gMonthInNum(month);
	}
	public void setDay(String day)
	{
		this.day = gDayInNum(day);
	}
	public MyDate()
	 {
		Calendar c = Calendar.getInstance();
		this.setDay(c.get(Calendar.DAY_OF_MONTH));
		this.setMonth(c.get(Calendar.MONTH)+1);
		this.setYear(c.get(Calendar.YEAR));
	 }
	public MyDate(int day, int month, int year) {
		super();
		this.day = day;
		this.month = month;
		this.year = year;
	}
	boolean ktnnhuan(int a) 
	{
		if((a%4==0)&&(a%100 != 0)) return true;
		if ((a%100==0)&&(a%400==0)) return true;
		return false;
	}
	boolean checkDate(int day,int month, int year)
	{
		if (year < 0) return false;
		if (month == 0 ) return false;
		if (month == 2)
		{
			if(ktnnhuan(year))
				{if(day<0||day>29) return false;}
			else if(day<0||day>28) return false;
		}
		if (month==4||month==6||month==9||month==11)
		{
			if(day <0||day>30) return false;
		}
		if(day<0||day>31) return false;
		return true;
		
	}
	public MyDate(String date)
	{
		String[] str = date.split(" ");
		this.year = Integer.parseInt(str[2]);
		String[] str1 = str[1].split("th");
		this.day = Integer.parseInt(str1[0]);
		this.month = gMonthInNum(str[0]);
		if(checkDate(this.day,this.month,this.year))System.out.println("Successful");
		else 
		{
			System.out.println("The date is invalid.");
			this.day=0;
			this.month=0;
			this.year=0;
		}
	}
	void print()
	{
		MyDate My_Date = new MyDate();
		System.out.println(">>The current date is : " +My_Date.getDay()+"/"+ My_Date.getMonth()+"/"+My_Date.getYear());
		
	}
	void printDate()
	{
		if(checkDate(day,month,year))
		System.out.println("Value of My Date is:  "+day+"/"+month+"/"+year);
		else
		{
			System.out.println("The date is invalid.");
		}
	}
	void Accept()
	{
		boolean check = true;
		while(check)
		{
		try
		{
		String Datein;
		System.out.print(">> Enter the date (Eg. February 18th 2019): ");
	    Datein = input.nextLine();
	    String[] str = Datein.split(" ");
		this.year = Integer.parseInt(str[2]);
		String[] str1 = str[1].split("th");
		this.day = Integer.parseInt(str1[0]);
		this.month = gMonthInNum(str[0]);
		if(checkDate(this.day,this.month,this.year)) 
		{
			System.out.println("Set corresponding value successfully!");
			printDate();
		}
		else 
		{
			System.out.println("The date is invalid.");
			this.day=0;
			this.month=0;
			this.year=0;
		}
	    System.out.println("Continue?(n or N to exit):  ");
		String c;
		c = input.nextLine();
		if((c.equals("n"))||(c.equals("N"))) check =false;
		}catch(Exception e)
		{
			System.err.println("False form.Continue?(n or N to exit):  ");
			String c;
			c = input.nextLine();
			if((c.equals("n"))||(c.equals("N"))) check =false;
		}
		}
	}
	public void printDateFormat()
	{
		Scanner input = new Scanner(System.in);
		int n;
		while(true)
		{
		Date date = new Date();
		System.out.println("");
		System.out.println(">> Select format of current date which you want to display:");
		System.out.println("1.dd/MM/yy");
		System.out.println("2.dd-MM-yy");
		System.out.println("3.yyyy-MM-dd");
		System.out.println("4.dd/MMM/yyyy");
		System.out.println("5.Day and Month in String.");
		System.out.println("6.Exit");
		System.out.print("Your selection: ");
		n=input.nextInt();
		switch(n)
		{
		case 1:
		{
			DateFormat df = new SimpleDateFormat("dd/MM/yy"); 
			String dateString  = df.format(date);
			System.out.println("The current date is : " + dateString);
			break;
		}
		case 2:
		{
			DateFormat df = new SimpleDateFormat("dd-MM-yy"); 
			String dateString  = df.format(date);
			System.out.println("The current date is : " + dateString);
			break;
		}
		case 3:
		{
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
			String dateString  = df.format(date);
			System.out.println("The current date is : " + dateString);
			break;
		}
		case 4:
		{
			DateFormat df = new SimpleDateFormat("dd/MMM/yyyy"); 
			String dateString  = df.format(date);
			System.out.println("The current date is : " + dateString);
			break;
		}
		case 5:
		{
			Calendar c = Calendar.getInstance();
			System.out.println("The current date is: " + gDayInStr(c.get(Calendar.DAY_OF_MONTH))+" "+gMonthInStr(c.get((Calendar.MONTH)+1))+" "+c.get(Calendar.YEAR));
			break;
		}
		case 6:
		{
			System.out.println("Exit.");
			break;
		}
		}
		input.close();
		if(n==6) break;
		}
	}
	int gMonthInNum(String a)
	{
		if (a.compareToIgnoreCase("january")==0)   return 1;
		if (a.compareToIgnoreCase("february")==0)  return 2;
		if (a.compareToIgnoreCase("march")==0)     return 3;
		if (a.compareToIgnoreCase("april")==0)     return 4;
		if (a.compareToIgnoreCase("may")==0)       return 5;
		if (a.compareToIgnoreCase("june")==0)      return 6;
		if (a.compareToIgnoreCase("july")==0)      return 7;
		if (a.compareToIgnoreCase("august")==0)    return 8;
		if (a.compareToIgnoreCase("september")==0) return 9;
		if (a.compareToIgnoreCase("october")==0)   return 10;
		if (a.compareToIgnoreCase("november")==0)  return 11;
		if (a.compareToIgnoreCase("december")==0)  return 12;
		return 0;
	}
	String gMonthInStr(int a)
	{
		if(a==1) return "January";
		if(a==2) return "February";
		if(a==3) return "March";
		if(a==4) return "April";
		if(a==5) return "May";
		if(a==6) return "June";
		if(a==7) return "July";
		if(a==8) return "August";
		if(a==9) return "September";
		if(a==10) return "October";
		if(a==11) return "November";
		if(a==12) return "December";
		return "Invalid";
	}
	int gDayInNum(String a)
	{
		if (a.compareToIgnoreCase("thousands")==0)   return 0;
		if (a.compareToIgnoreCase("first")==0)       return 1;
		if (a.compareToIgnoreCase("second")==0)      return 2;
		if (a.compareToIgnoreCase("third")==0)       return 3;
		if (a.compareToIgnoreCase("fouth")==0)       return 4;
		if (a.compareToIgnoreCase("fifth")==0)       return 5;
		if (a.compareToIgnoreCase("sixth")==0)       return 6;
		if (a.compareToIgnoreCase("seventh")==0)     return 7;
		if (a.compareToIgnoreCase("eighth")==0)      return 8;
		if (a.compareToIgnoreCase("ninth")==0)       return 9;
		if (a.compareToIgnoreCase("tenth")==0)       return 10;
		if (a.compareToIgnoreCase("eleventh")==0)    return 11;
		if (a.compareToIgnoreCase("twelfth")==0)     return 12;
		if (a.compareToIgnoreCase("thirteenth")==0)   return 13;
		if (a.compareToIgnoreCase("fourteenth")==0)   return 14;
		if (a.compareToIgnoreCase("fifteenth")==0)    return 15;
		if (a.compareToIgnoreCase("sixteenth")==0)    return 16;
		if (a.compareToIgnoreCase("seventeenth")==0)  return 17;
		if (a.compareToIgnoreCase("eighteenth")==0)   return 18;
		if (a.compareToIgnoreCase("neneteenth")==0)   return 19;
		if (a.compareToIgnoreCase("twentith")==0)     return 20;
		if (a.compareToIgnoreCase("twenty-first")==0) return 21;
		if (a.compareToIgnoreCase("twenty-second")==0)  return 22;
		if (a.compareToIgnoreCase("twenty-third")==0)   return 23;
		if (a.compareToIgnoreCase("twenty-fourth")==0)  return 24;
		if (a.compareToIgnoreCase("twenty-fifth")==0)   return 25;
		if (a.compareToIgnoreCase("twenty-sixth")==0)   return 26;
		if (a.compareToIgnoreCase("twenty-seventh")==0) return 27;
		if (a.compareToIgnoreCase("twenty-eighth")==0)  return 28;
		if (a.compareToIgnoreCase("twenty-nineth")==0)  return 29;
		if (a.compareToIgnoreCase("thirtieth")==0)      return 30;
		if (a.compareToIgnoreCase("thirty-first")==0)   return 31;
		return 0;
	}
	String gDayInStr(int a)
	{
		switch(a)
		{
		case 0: return "Thousand";
		case 1: return "First";
		case 2: return "Second";
		case 3: return "Third";
		case 4: return "Four";
		case 5: return "Five";
		case 6: return "Six";
		case 7: return "Seven";
		case 8: return "Eight";
		case 9: return "Nine";
		case 10: return "Ten";
		case 11: return "Eleven";
		case 12: return "Twelve";
		case 13: return "Thirteen";
		case 14: return "Fourteen";
		case 15: return "Fifteen";
		case 16: return "Sixteen";
		case 17: return "Seventeen";
		case 18: return "Eighteen";
		case 19: return "Nineteen";
		case 20: return "Twenty";
		case 21 :return "Twenty-One";
		case 22: return "Twenty-Two";
		case 23: return "Twenty-Three";
		case 24: return "Twenty-Four";
		case 25: return "Twenty-Five";
		case 26: return "Twenty-Six";
		case 27: return "Twenty-Seven";
		case 28: return "Twenty-Eight";
		case 29: return "Twenty-Nine";
		case 30: return "Thirty";
		case 31: return "Thirty-One";
		}
		return "Invaild.";
		}
	}
