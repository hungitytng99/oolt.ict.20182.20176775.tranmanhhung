package soict.hust.ictglobal.gui.awt;

import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class AWTCounterNamedInnerClass extends Frame {
	private TextField tfCount;
	private Button btnCount;
	private int count = 0;

	public AWTCounterNamedInnerClass() {
		setLayout(new FlowLayout()); // "super" Frame sets to FlowLayout
		add(new Label("Counter")); // An anonymous instance of Label
		tfCount = new TextField("0", 10);
		tfCount.setEditable(false); // read-only
		add(tfCount); // "super" Frame adds tfCount

		btnCount = new Button("Count");
		add(btnCount); // "super" Frame adds btnCount

		// Construct an anonymous instance of BtnCountListener (a named inner class).
		// btnCount adds this instance as a ActionListener.
		btnCount.addActionListener(evt -> tfCount.setText(++count + ""));
		this.addWindowListener(new WinExitListener());
		setTitle("AWT Counter");
		setSize(250, 100);
		setVisible(true);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new AWTCounterNamedInnerClass();
	}
	private class WinExitListener implements WindowListener
	{

		@Override
		public void windowActivated(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowClosed(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowClosing(WindowEvent arg0) {
			// TODO Auto-generated method stub
			System.exit(0);
		}

		@Override
		public void windowDeactivated(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowDeiconified(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowIconified(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowOpened(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}

	

}
