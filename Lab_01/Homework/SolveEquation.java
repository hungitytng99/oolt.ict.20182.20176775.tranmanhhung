import java.util.Scanner;
import java.lang.Math;
public class SolveEquation
{
public static void main(String argv[])
{
	Scanner sc = new Scanner(System.in);
	double a,b,c,x;
	System.out.println("Solve first degree equation with one variable(ax+b=c):");
	System.out.print("Input a = ");
	a=sc.nextDouble();
	System.out.print("Input b = ");
	b=sc.nextDouble();
	System.out.print("Input c = ");
	c=sc.nextDouble();
	if(a != 0 ) 
	{
		x=(c-b)/a;
		System.out.println("Solution: x=  "+ x);
	}
	else 
	{
	if((c-b)!=0) System.out.println("The equation has no solution.");
	if((c-b)==0) System.out.println("The equation has many solutions.");
	}
	System.out.println("Solve first degree equation with two variables(ax+by=c):");
	System.out.print("Input a = ");
	a=sc.nextDouble();
	System.out.print("Input b = ");
	b=sc.nextDouble();
	System.out.print("Input c = ");
	c=sc.nextDouble();
	if((a != 0)&&(b != 0 )) 
	{
		System.out.println("x belong to R");
		System.out.println("y = "+(-a/b) + "x +"+ (c/b));
	}
	else if((a !=0)&&(b==0)&&(c!=0)) 
	{
		System.out.println("y belong to R");
		System.out.println("x = " +(c/a));
		
	}
	else if((a!=0)&&(b==0)&&(c==0))
	{
		System.out.println("y belong to R");
		System.out.println("x = 0");
	}
	else if((a==0)&&(b!=0)&&(c!=0))
	{
		System.out.println("x belong to R");
		System.out.println("y = " + (c/b));
	}
	else
	{
		System.out.println("x belong to R");
		System.out.println("y = 0");
	}
	System.out.println("Solve second degree equation with one variable(ax^2+bx+c=0):");
	System.out.print("Input a = ");
	a=sc.nextDouble();
	System.out.print("Input b = ");
	b=sc.nextDouble();
	System.out.print("Input c = ");
	c=sc.nextDouble();
	if(a==0)
	{
		if(b==0) System.out.println("The equation has no solution");
		else System.out.println("The solution: x = " + (-c/b));
		System.exit(0);
	}
	double denta;
	denta = b*b-4*a*c;
	System.out.println(denta);
	if( denta < 0) 
		System.out.println("The equation has no root");
	else if ( denta==0)
		System.out.println("The equation has double root: x = " + (-b/(2*a)));
	else
	{
		System.out.println("The equation has two roots:");
		System.out.println("x1 = " + (((-b)+Math.sqrt(denta))/(2*a)));
		System.out.println("x2 = " + (((-b)-Math.sqrt(denta))/(2*a)));
	}
}
}