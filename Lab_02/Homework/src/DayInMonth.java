import java.util.Scanner;
public class DayInMonth {
	public static boolean checkYear(int year)
	{
		if((year%4 == 0)&&(year%100 != 0)) return true;
		if((year%100 == 0)&&(year%400 == 0)) return true;
		return false;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int month=0,year=0;
		while((month<1)||(month>12))
		{
			System.out.print("Input the month(from [1..12]):  ");
			month=sc.nextInt();
			if((month<1)||(month>12)) System.out.println(">> Input again: ");
		}
		while(year<1)
		{
			System.out.print("Input the year(year > 0):  ");
			year=sc.nextInt();
			if(year<1) System.out.println(">> Input again: ");
		}
		switch (month)
		{
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			System.out.println("Month "+ month + " in "+ year+" have 31 days.");
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			System.out.println("Month "+ month + " in "+ year+" have 30 days.");
			break;
		case 2:
			if(checkYear(year)) System.out.println("Month "+ month + " in "+ year+" have 29 days.");
			else System.out.println("Month "+ month + " in "+ year+" have 28 days.");
			break;
		}
	}

}
