import java.util.Arrays;
import java.util.Scanner;
public class OperationWithArray {
	public static final int MAX=30;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc= new Scanner(System.in);
		int n=6;
		double Sum=0;
		double [] a = {6,8,7,4,5,3};
		System.out.println("The array: " + Arrays.toString(a));
		for(int i=0;i<n;i++)
			for(int j=i+1;j<n;j++)
			{
				if (a[j]<a[i]) 
				{
					double temp = a[j];
					a[j] = a[i];
					a[i] = temp;
				}
			}
		for(int i=0;i<n;i++) Sum+=a[i];
		System.out.println("The array after sorting: " + Arrays.toString(a));
		System.out.println("The Sum of array is : " + Sum);
		System.out.println("The Average of array is : "+ (double)(Sum/n));
		
	}

}
