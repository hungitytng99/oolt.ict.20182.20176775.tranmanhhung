import java.util.Scanner;
public class TriangleNStar {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Input n: ");
		int n = sc.nextInt();
		System.out.println(">> Triangle " + n +" Star :");
		for(int i=1;i<(n+1);i++)
		{
			for(int j=(n-i);j>0;j--) System.out.print(" ");
			for(int k=0;k<(2*i-1);k++) System.out.print("*");
			System.out.println("");
		}
	}

}
