
public class Order {
	public static final int MAX_NUMBER_ORDERED = 10;
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBER_ORDERED];
	private int qtyOrdered=0;
	public int getQtyOrdered() {
		return qtyOrdered;
	}
	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	boolean addDigitalVideoDisc(DigitalVideoDisc disc)
	{
		if(getQtyOrdered()>(MAX_NUMBER_ORDERED-1)) 
			{
			System.out.println("The ordered is almost full.The disc: " + disc.getTitle()+ "couldn't be added");
			return false;
			}
		itemsOrdered[qtyOrdered]=disc;
		qtyOrdered+=1;
		return true;
	}
	void removeDigitalVideoDisc(DigitalVideoDisc disc)
	{
		int i;
		for(i=0;i<qtyOrdered;i++)
		{
			if(itemsOrdered[i]==disc) break;
		}
		for(int j=i+1;j<qtyOrdered;i++,j++)
			itemsOrdered[i]=itemsOrdered[j];
		//itemsOrdered[qtyOrdered-1] = new DigitalVideoDisc();
		qtyOrdered--;
	}
	float totalCost()
	{
		float Sum=0;
		for(int j=0;j<qtyOrdered;j++) 
		{
			Sum = Sum + itemsOrdered[j].getCost();
			}
		return Sum;
	}
}
