
public class Order {
	public static final int MAX_NUMBER_ORDERED = 10;
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBER_ORDERED];
	private int qtyOrdered=0;
	public int getQtyOrdered() {
		return qtyOrdered;
	}
	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	private String  dateOrdered;
	public String getDateOrdered() {
		return dateOrdered;
	}
	public void setDateOrdered(String dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
	boolean addDigitalVideoDisc(DigitalVideoDisc disc)
	{
		if(getQtyOrdered()>(MAX_NUMBER_ORDERED-1)) 
			{
			System.out.println("The ordered is almost full.The disc: " + disc.getTitle()+ "couldn't be added");
			return false;
			}
		itemsOrdered[qtyOrdered]=disc;
		qtyOrdered+=1;
		return true;
	}
	boolean addDigitalVideoDisc(DigitalVideoDisc[] dvdList,int n)
	{
		for(int i=getQtyOrdered(),j=0;i<n;i++,j++)
		{
			if(i>(MAX_NUMBER_ORDERED-1)) 
			{
				System.out.println("The ordered is almost full.");
				for(int k=(i-MAX_NUMBER_ORDERED);k<n;k++)
				{
					System.out.println("The disc: "+ dvdList[k].getTitle() +"wasn't added.");
				}
				return false;
			}
			else {
			dvdList[j]=itemsOrdered[i];
			qtyOrdered++;
			}
		}
		return true;
	}
	void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2)
	{
		addDigitalVideoDisc(dvd1);
	    addDigitalVideoDisc(dvd2);
	}
	void removeDigitalVideoDisc(DigitalVideoDisc disc)
	{
		int i;
		for(i=0;i<qtyOrdered;i++)
		{
			if(itemsOrdered[i]==disc) break;
		}
		for(int j=i+1;j<qtyOrdered;i++,j++)
			itemsOrdered[i]=itemsOrdered[j];
		//itemsOrdered[qtyOrdered-1] = new DigitalVideoDisc();
		qtyOrdered--;
	}
	float totalCost()
	{
		float Sum=0;
		for(int j=0;j<qtyOrdered;j++) 
		{
			Sum = Sum + itemsOrdered[j].getCost();
			}
		return Sum;
	}
	void printOrders()
	{
		System.out.println("*************************Orders***************************");
		System.out.println("Date : " + dateOrdered);
		System.out.println("Orders Items:");
		for(int i=0;i<qtyOrdered;i++)
		{
			System.out.println((i+1) +". DVD - "+itemsOrdered[i].getTitle()+" - "+itemsOrdered[i].getCategory()+" - "+itemsOrdered[i].getDirector()+" - "+itemsOrdered[i].getLength()+": "+itemsOrdered[i].getCost()+"$"); 
		}
		System.out.println("Total cost: " + totalCost());
		System.out.println("************************************************************");
	}
}
