import java.util.Scanner;

public class DateTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyDate My_Date1 = new MyDate(27,11,1999);
		My_Date1.printDate();
		My_Date1.setMonth("february");
		My_Date1.setDay("First");
		//setYear in String too long :)
		My_Date1.printDate();
		MyDate My_Date2 = new MyDate();
		My_Date2.printDate();
		My_Date2.print();
		
		MyDate My_Date3 = new MyDate();
		My_Date3.Accept();
		My_Date3.printDateFormat();//print Format of current Date, or Date is inputed from keyboard is easier...

		//COMPARE TWO DATE: 
		DateUtils DateUtil = new DateUtils();
		String dateA,dateB;
		Scanner input = new Scanner(System.in); 
		System.out.println("");
		System.out.println(">>COMPARE TWO DATE:");
		System.out.println("Date in the form dd/MM/yyyy. Eg. 27/11/1999");
		System.out.print("The first date: ");
		dateA = input.nextLine();
		System.out.print("The second date: ");
		dateB = input.nextLine();
		switch(DateUtil.Compare2Dates(dateA,dateB))
		{
		case (-1): 
			System.out.println(">> "+  dateA + " is before " + dateB + "." );
			break;
		case (0):
			System.out.println(">> "+  dateA + " is the same as " + dateB +".");
			break;
		case (1): 
			System.out.println(">> "+  dateA + " is after " + dateB + ".");
			break;
		case (2):
			break;
		}
		input.close();
		
		//SORT DATES:
		DateUtil.SortDate();
		System.exit(0);
	}

}
