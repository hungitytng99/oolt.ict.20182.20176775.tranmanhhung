import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
public class DateUtils {
	public int Compare2Dates(String dateA,String dateB) 
	{
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		Date date1,date2;
		try
		{
		date1 = df.parse(dateA);
        date2 = df.parse(dateB);
		}catch(Exception e)
		{
			System.err.println("False form.");
			return 2;
		}
		return (date1.compareTo(date2));
	}
	
	public void SortDate()
	{
		final int MAX_DATE =20;
		Scanner input = new Scanner(System.in);
		System.out.println(">>>>> SORT DATE(INCREASED GRADUATELY)<<<<<");
		String [] Date = new String[MAX_DATE];
		int n;
		System.out.print("Input the amount of date: " );
		n = input.nextInt();
		System.out.println(" Input the date: ");
		String clearNewLine = input.nextLine();
		for(int i=0;i<n;i++)
		{
			System.out.print(">> Date " + (i+1) + ": ");
			Date[i] = input.nextLine();
		}
		for(int i=0;i<n;i++)
		{
			for(int j=(i+1);j<n;j++)
			{
				if(Compare2Dates(Date[i], Date[j])== 1 ) 
				{
					String temp = Date[i];
					Date[i] = Date[j];
					Date[j] = temp;
				}
			}
		}
		System.out.println(">>>  DATE AFTER SORTING: ");
		for (int i=0;i<n;i++)
			System.out.println(Date[i]);
		input.close();
	}
}
